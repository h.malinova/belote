package belote;

public class Card {
	private Rank rank;
	private Suit suit;

	Card(Rank myRank, Suit mySuit) {
		rank = myRank;
		suit = mySuit;
	}

	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}
}
