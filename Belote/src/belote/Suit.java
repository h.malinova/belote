package belote;

public enum Suit {
	CLUBS("♣"), DIAMONDS("♦"), HEARTS("♥"), SPADES("♠");

	private String value;

	private Suit(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
