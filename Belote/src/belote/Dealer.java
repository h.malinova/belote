package belote;

import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

public class Dealer {
	private Stack<Card> deck; // TODO ArrayDequeue

	// Fill the deck
	public Dealer() {
		deck = new Stack<Card>();
		for (Rank rank : Rank.values()) {
			for (Suit suit : Suit.values()) {
				deck.push(new Card(rank, suit));
			}
		}
		Collections.shuffle(deck, new Random());
	}

	// TODO opravi
	public void cutTheDeck(int cutPoint) {
		for (int i = 0; i < cutPoint; i++) {
			deck.add(deck.pop());
		}
	};

	// TODO
	public void dealBeforeBidding(Queue<Player> players) {
	}; // - x3 x2

	public void dealAfterBidding(Queue<Player> players) {
	}; // - x3

	public void receiveCards(List<Card> card) {
	};
}
