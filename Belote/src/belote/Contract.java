package belote;

public enum Contract {
	CLUBS, DIAMONDS, HEARTS, SPADES, NO_TRUMPS, ALL_TRUMPS, PASS;
}
