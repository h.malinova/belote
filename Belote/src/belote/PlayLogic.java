package belote;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PlayLogic {
	private Contract contract;

	private Card getHighestCard(Collection<Card> cards, Suit wantedSuit) {
		return Collections.max(cards.stream().filter(p -> p.getSuit().equals(wantedSuit)).collect(Collectors.toList()),
				(p1, p2) -> CardValues.getValueOfCard(p1, contract) - CardValues.getValueOfCard(p1, contract));
	}

	private boolean isPlayingOnSuit() {
		if (contract.equals(Contract.CLUBS) || contract.equals(Contract.DIAMONDS) || contract.equals(Contract.HEARTS)
				|| contract.equals(Contract.SPADES)) {
			return true;
		}
		return false;
	}

	private List<Card> filterTrumps(Collection<Card> cards) {
		return cards.stream().filter(p -> CardValues.correspondingSuitToContract.get(p.getSuit()).equals(contract))
				.collect(Collectors.toList());
	}

	private List<Card> filterValidForSuit(Map<Player, Card> cardsOnTable, List<Card> cardsInHand, Suit wantedSuit) {
		List<Card> trumpsOnTable = filterTrumps(cardsOnTable.values());
		List<Card> trumpsInHand = filterTrumps(cardsInHand);
		if (trumpsInHand.isEmpty()) {
			return cardsInHand;
		}
		if (trumpsOnTable.isEmpty()) {
			return trumpsInHand;
		}
		Card highestTrumpOnTable = getHighestCard(cardsOnTable.values(),
				CardValues.correspondingContractToSuit.get(contract));
		Card highestTrumpInHand = getHighestCard(cardsOnTable.values(),
				CardValues.correspondingContractToSuit.get(contract));
		if (CardValues.getValueOfCard(highestTrumpInHand, contract) > CardValues.getValueOfCard(highestTrumpOnTable,
				contract)) {
			return filterValidForRaising(trumpsInHand, highestTrumpOnTable);
		}
		return cardsInHand;

	}

	private boolean mustRaise(Suit wantedSuit) {
		if (contract.equals(Contract.ALL_TRUMPS)
				|| contract.equals(CardValues.correspondingSuitToContract.get(wantedSuit))) {
			return true;
		} else {
			return false;
		}
	}

	private List<Card> filterValidForRaising(List<Card> cards, Card currentHighest) {
		int currentHighestValue = CardValues.getValueOfCard(currentHighest, contract);
		List<Card> validCards = cards.stream().filter(p -> CardValues.getValueOfCard(p, contract) > currentHighestValue)
				.collect(Collectors.toList());
		if (validCards.isEmpty()) {
			return cards;
		}
		return validCards;
	}

	public List<Card> getValidCards(Map<Player, Card> cardsOnTable, List<Card> cardsInHand, Suit wantedSuit) {
		List<Card> cardsOfWantedSuit = cardsInHand.stream().filter(p -> p.getSuit().equals(wantedSuit))
				.collect(Collectors.toList());
		if (cardsOfWantedSuit.isEmpty()) {
			if (isPlayingOnSuit()) {
				return filterValidForSuit(cardsOnTable, cardsInHand, wantedSuit);
			} else {
				return cardsInHand;
			}
		} else {
			if (mustRaise(wantedSuit)) {
				Card currentHighest = getHighestCard(cardsOnTable.values(), wantedSuit);
				return filterValidForRaising(cardsOfWantedSuit, currentHighest);
			} else {
				return cardsOfWantedSuit;
			}
		}
	}

	private void prepareCardsForDeclarations(List<Card> cards) {
		if (isPlayingOnSuit()) {
			cards = cards.stream().filter(p -> CardValues.correspondingSuitToContract.get(p.getSuit()).equals(contract))
					.collect(Collectors.toList());
		}
		Collections.sort(cards, Comparator.comparing(Card::getSuit).thenComparing(Card::getRank));
	}

	private void addSequentialAnnounce(int sequenceLength, Rank highestRank, LinkedList<Announce> announces) {
		switch (sequenceLength) {
		case 3:
			announces.add(new Announce(Declaration.TIERCE, highestRank));
			break;
		case 4:
			announces.add(new Announce(Declaration.QUARTE, highestRank));
			break;
		case 5:
		case 6:
		case 7:
			announces.add(new Announce(Declaration.QUINTE, highestRank));
			break;
		case 8:
			announces.add(new Announce(Declaration.TIERCE, Rank.NINE));
			announces.add(new Announce(Declaration.QUINTE, highestRank));
			break;
		}
	}

	private void addSequentialAnnounces(List<Card> cards, LinkedList<Announce> announces) {
		int sequenceLength = 0;
		Iterator<Card> i = cards.iterator();
		Card previousCard = i.next();
		while (i.hasNext()) {
			Card currentCard = i.next();
			if (currentCard.getSuit().equals(previousCard.getSuit())
					&& currentCard.getRank().ordinal() - previousCard.getRank().ordinal() == 1) {
				sequenceLength++;
			} else {
				addSequentialAnnounce(sequenceLength, previousCard.getRank(), announces);
				sequenceLength = 1;
			}
			previousCard = currentCard;
		}
		addSequentialAnnounce(sequenceLength, previousCard.getRank(), announces);
	}

	private boolean containsForOfAKind(List<Card> cards, Rank rank) {
		int counter = 0;
		for (Card card : cards) {
			if (card.getRank().equals(rank)) {
				counter++;
			}
		}
		return counter == 4;
	}

	private void addCarre(Rank rank, LinkedList<Announce> announces) {
		switch (rank) {
		case NINE:
			announces.add(new Announce(Declaration.CARRE_9, rank));
			break;
		case JACK:
			announces.add(new Announce(Declaration.CARRE_J, rank));
			break;
		case TEN:
		case QUEEN:
		case KING:
		case ACE:
			announces.add(new Announce(Declaration.CARRE, rank));
			break;
		default:
			break;
		}
	}

	private void addCarres(List<Card> cards, LinkedList<Announce> announces) {
		for (Rank rank : Rank.values()) {
			if (containsForOfAKind(cards, rank)) {
				addCarre(rank, announces);
			}
		}
	}

	public List<Announce> getDeclarations(List<Card> cardsInHand) {
		if (contract.equals(Contract.NO_TRUMPS)) {
			return new LinkedList<Announce>();
		}
		prepareCardsForDeclarations(cardsInHand);
		LinkedList<Announce> announces = new LinkedList<>();
		addSequentialAnnounces(cardsInHand, announces);
		addCarres(cardsInHand, announces);
		return announces;
	}

}
