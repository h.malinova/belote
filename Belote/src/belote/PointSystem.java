package belote;

import java.util.Collection;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

public class PointSystem {

	private List<Player> firstTeam; 
	private static short currentScoreTeam1 = 0;
	private static short globalScoreTeam1 = 0;
	private List<Player> secondTeam; 
	private static short currentScoreTeam2 = 0;
	private static short globalScoreTeam2 = 0;
	
	private Contract currentContract;

	public PointSystem() {
		firstTeam = new ArrayList<>();
		secondTeam = new ArrayList<>();
	};

	public Contract getContract () {
		return currentContract;
	}
	
	public void setContract(Contract contract) {
		currentContract = contract;
	};

	public String showTeams() {
		return 	firstTeam.get(0).getName() + " " + firstTeam.get(1).getName() + " " + 
				secondTeam.get(0).getName() + " " +  secondTeam.get(1).getName();
	}
	
	public void setTeams(Collection<Player> players) {
		for (Player player : players) {
			boolean result = player.getTeam().equals(Team.FIRST_TEAM) ? 
					firstTeam.add(player) : secondTeam.add(player);	
		}
 	};

	// TODO 
	
	// da vzeme vsichki anonsi i da preceni koi vaji
	// private void processDeclarations (List <Pair <Declaration, Rank> >);  

 	
	public void calculatePoints (List<Card> firstTeamCards, List<Card> secondTeamCards, 
			List<Declaration> firstTeamDeclarations, List<Declaration> secondTeamDeclarations,
			Team lastTen) {
		//karti
		//10
		//valat
		
		// eventualno x2 (bez koz)
		
		// processDeclaration
		addToGlobalScore();
	};	

	private void addToGlobalScore() {};
}
