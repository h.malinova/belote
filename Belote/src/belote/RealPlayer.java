package belote;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class RealPlayer extends Player {
	RealPlayer(String myName) {};

	@Override
	public int cutDeck() {
		Scanner in = new Scanner(System.in);
		System.out.println("Choose a position to cut the deck in: ");
		int position = in.nextInt();
		in.close();
		return position;
	};

	@Override
	public Card playCard(Map<Player, Card> cardsOnTable, Suit wantedSuit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Contract bid(Contract currentHighest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Declaration> announce() {
		// TODO Auto-generated method stub
		return null;
	}
}
