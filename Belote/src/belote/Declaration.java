package belote;

public enum Declaration {

	TIERCE(20), QUARTE(50), QUINTE(100), CARRE_J(200), CARRE_9(150), 
	CARRE(100), BELOTE(20), NONE(0);

	private final int points;

	Declaration(int points) {
		this.points = points;
	}

	int getPoints() {
		return points;
	}
}
