package belote;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class Player {
	protected String name;
	protected Team team;
	protected Hand myHand;
	protected PlayLogic myLogic;
	

	public String getName() {
		return name;
	}

	public Team getTeam() {
		return team;
	}

	// TODO
	public void receiveCards(List<Card> cards) {
		myHand.addCards(cards);
	};

	public abstract int cutDeck();

	// TODO
	// returns all of the cards in the hand to the deck
	public List<Card> returnCards() {
		return new ArrayList<Card>();
	};

	// check has belote
	// TODO vzima MAP, shtoto kak da znae dali e zaduljen da caka?
	public abstract Card playCard(Map<Player, Card> cardsOnTable, Suit wantedSuit);

	public abstract Contract bid(Contract currentHighest);

	public abstract List<Declaration> announce();
}
