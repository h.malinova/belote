package belote;

public enum Rank {
	SEVEN("7"), EIGHT("8"), NINE("9"), TEN("10"), JACK("J"), QUEEN("Q"), KING("K"), ACE("A");

	private String value;

	private Rank(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
