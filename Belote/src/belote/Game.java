package belote;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;

public class Game {
	Queue<Player> players;
	Dealer dealer;
	PointSystem pointSystem;

	List<Card> firstTeamCards;
	List<Card> secondTeamCards;
	List<Declaration> firstTeamDeclarations;
	List<Declaration> secondTeamDeclarations;

	// TODO opravi porednostta na vkluchvane v opashkata
	// set team
	Game(String nameOfPlayer) {
		Bot east = new Bot("East");
		players.offer(east);
		Bot north = new Bot("North");
		players.offer(north);
		Bot west = new Bot("West");
		players.offer(west);
		Bot player = new Bot(nameOfPlayer);
		players.offer(player);
		dealer = new Dealer();
		pointSystem = new PointSystem();
	}

	public Contract chooseContract() {
		return Contract.PASS;
		// pich cepi; razdavam karti; shte ima li Contract?
		// 3 posledovatelni pass / OR / vsichkoKoz (TODO kontra/rekontra)
	}

	private Player getWinner(Map<Player, Card> cardsOnTable) {
		Player winner = null;
		int maxPoints = -1;
		for (Player p : cardsOnTable.keySet()) {
			int currentPoints = CardValues.getValueOfCard(cardsOnTable.get(p), pointSystem.getContract());
			if (currentPoints > maxPoints) {
				maxPoints = currentPoints;
				winner = p;
			}
		}
		return winner;
	}

	private Player playTrick() {
		Player currentPlayer = players.poll();
		Map<Player, Card> cardsOnTable = new HashMap<>();
		Suit wantedSuit = null;

		cardsOnTable.put(currentPlayer, currentPlayer.playCard(cardsOnTable, wantedSuit));
		wantedSuit = cardsOnTable.get(currentPlayer).getSuit();
		players.offer(currentPlayer);

		for (int i = 0; i < 3; i++) {
			currentPlayer = players.poll();
			cardsOnTable.put(currentPlayer, currentPlayer.playCard(cardsOnTable, wantedSuit));
			players.offer(currentPlayer);
		}

		return getWinner(cardsOnTable);
	}

//	private void playRound();
	// grija za opashka;

//	public void playGame();

}
