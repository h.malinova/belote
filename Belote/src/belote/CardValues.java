package belote;

import java.util.Map;
import java.util.HashMap;

public class CardValues {

	public static Map<Rank, Integer> pointsOfCardsOnTrumps;
	static {
		pointsOfCardsOnTrumps = new HashMap<>();
		pointsOfCardsOnTrumps.put(Rank.SEVEN, 0);
		pointsOfCardsOnTrumps.put(Rank.EIGHT, 0);
		pointsOfCardsOnTrumps.put(Rank.QUEEN, 3);
		pointsOfCardsOnTrumps.put(Rank.KING, 4);
		pointsOfCardsOnTrumps.put(Rank.TEN, 10);
		pointsOfCardsOnTrumps.put(Rank.ACE, 11);
		pointsOfCardsOnTrumps.put(Rank.NINE, 14);
		pointsOfCardsOnTrumps.put(Rank.JACK, 20);
	}

	public static Map<Rank, Integer> pointsOfCardsOnNoTrumps;
	static {
		pointsOfCardsOnNoTrumps = new HashMap<>();
		pointsOfCardsOnNoTrumps.put(Rank.SEVEN, 0);
		pointsOfCardsOnNoTrumps.put(Rank.EIGHT, 0);
		pointsOfCardsOnNoTrumps.put(Rank.NINE, 0);
		pointsOfCardsOnNoTrumps.put(Rank.JACK, 2);
		pointsOfCardsOnNoTrumps.put(Rank.QUEEN, 3);
		pointsOfCardsOnNoTrumps.put(Rank.KING, 4);
		pointsOfCardsOnNoTrumps.put(Rank.TEN, 10);
		pointsOfCardsOnNoTrumps.put(Rank.ACE, 11);
	}

	public static Map<Suit, Contract> correspondingSuitToContract;
	static {
		correspondingSuitToContract = new HashMap<>();
		correspondingSuitToContract.put(Suit.CLUBS, Contract.CLUBS);
		correspondingSuitToContract.put(Suit.DIAMONDS, Contract.DIAMONDS);
		correspondingSuitToContract.put(Suit.HEARTS, Contract.HEARTS);
		correspondingSuitToContract.put(Suit.SPADES, Contract.SPADES);
	}

	public static Map<Contract, Suit> correspondingContractToSuit;
	static {
		correspondingContractToSuit = new HashMap<>();
		correspondingContractToSuit.put(Contract.CLUBS, Suit.CLUBS);
		correspondingContractToSuit.put(Contract.DIAMONDS, Suit.DIAMONDS);
		correspondingContractToSuit.put(Contract.HEARTS, Suit.HEARTS);
		correspondingContractToSuit.put(Contract.SPADES, Suit.SPADES);
	}

	public static int getValueOfCard(Card card, Contract contract) {
		if (contract.equals(Contract.ALL_TRUMPS) || contract.equals(correspondingSuitToContract.get(card.getSuit()))) {
			return pointsOfCardsOnTrumps.get(card.getRank());
		} else {
			return pointsOfCardsOnNoTrumps.get(card.getRank());
		}
	}
}