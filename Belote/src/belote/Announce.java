package belote;

public class Announce {
	private Declaration declaration;
	private Rank highestRank;

	public Announce(Declaration declaration, Rank highestRank) {
		this.declaration = declaration;
		this.highestRank = highestRank;
	}

	public Declaration getDeclaration() {
		return declaration;
	}

	public Rank getHighestRank() {
		return highestRank;
	}
}
