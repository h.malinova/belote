package stubs;

import java.util.List;
import java.util.Map;

import belote.Card;
import belote.Contract;
import belote.Declaration;
import belote.Player;
import belote.Suit;
import belote.Team;

public class PlayerStub extends Player{

	PlayerStub(String name, Team team) {
		this.name = name;
		this.team = team;
	}
	
	@Override
	public int cutDeck() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Card playCard(Map<Player, Card> cardsOnTable, Suit wantedSuit) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Contract bid(Contract currentHighest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Declaration> announce() {
		// TODO Auto-generated method stub
		return null;
	}
}
